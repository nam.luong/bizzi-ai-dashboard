import pickle
import collections

from datetime import datetime
import pandas as pd

pd.options.mode.chained_assignment = None


def simplify_date(date):
    date = date.split("T")[0]
    date = datetime.strptime(date, "%Y-%m-%d")
    date = date.strftime("%m/%Y")
    return date


def compare_date(date_in, date_anchor):
    return datetime.strptime(date_in, "%m/%Y") >= datetime.strptime(
        date_anchor, "%m/%Y"
    )


def load_pickle_data(pickle_file):
    with open(pickle_file, "rb") as handle:
        combination_dict = pickle.load(handle)
    buyer_dict = dict(combination_dict["buyer_dict"])
    seller_dict = dict(combination_dict["seller_dict"])
    taxcode_to_name = dict(combination_dict["taxcode_to_name"])

    list_keys = list(buyer_dict.keys())
    list_amount = [buyer_dict[key]["total_payment"] for key in list_keys]
    zip_iterator = zip(list_keys, list_amount)
    buyer_with_amount = dict(zip_iterator)
    sorted_buyer_with_amount = dict(
        sorted(buyer_with_amount.items(), key=lambda item: item[1], reverse=True)
    )

    list_keys = list(seller_dict.keys())
    list_amount = [seller_dict[key]["total_payment"] for key in list_keys]
    zip_iterator = zip(list_keys, list_amount)
    seller_with_amount = dict(zip_iterator)
    sorted_seller_with_amount = dict(
        sorted(seller_with_amount.items(), key=lambda item: item[1], reverse=True)
    )

    dict_for_year = collections.defaultdict(dict)
    for buyer_key in buyer_dict.keys():
        buyer = buyer_dict[buyer_key]
        amount_2020 = 0
        amount_2021 = 0
        for seller in buyer.keys():
            if seller != "total_payment":
                dates = buyer[seller]["dates"]
                for date in dates.keys():
                    if compare_date(date, "01/2021"):
                        amount_2021 += buyer[seller]["dates"][date][0]
                    else:
                        amount_2020 += buyer[seller]["dates"][date][0]
        dict_for_year[buyer_key] = {
            "2020": amount_2020,
            "2021": amount_2021,
        }

    return (
        buyer_dict,
        seller_dict,
        taxcode_to_name,
        sorted_buyer_with_amount,
        sorted_seller_with_amount,
        dict_for_year,
    )


list_simplify = [
    "CÔNG TY CỔ PHẦN THƯƠNG MẠI VÀ DỊCH VỤ",
    "CÔNG TY TNHH SẢN XUẤT VÀ THƯƠNG MẠI",
    "CÔNG TY TNHH MỘT THÀNH VIÊN",
    "CÔNG TY TRÁCH NHIỆM HỮU HẠN",
    "CÔNG TY TNHH THƯƠNG MẠI DỊCH VỤ",
    "CÔNG TY CỔ PHẦN VẬN TẢI",
    "CÔNG TY CỔ PHẦN DỊCH VỤ",
    "CÔNG TY CỔ PHẦN",
    "CÔNG TY CP",
    "CÔNG TY TNHH",
    "CÔNG TY",
    "CHI NHÁNH",
]


def simplify(name, list_simplify=list_simplify):
    if name == None:
        name = "unknown"
    name = name.upper()
    for i in list_simplify:
        if i in name:
            name = name.replace(i, "")
            name = name.replace("  ", " ")
    return name.strip()


defaut_list_date = [
    "11/2021",
    "08/2020",
    "08/2021",
    "01/2021",
    "04/2021",
    "09/2021",
    "03/2021",
    "04/2020",
    "05/2021",
    "05/2020",
    "07/2020",
    "10/2020",
    "06/2020",
    "09/2020",
    "03/2022",
    "03/2020",
    "12/2021",
    "01/2020",
    "02/2021",
    "11/2020",
    "12/2020",
    "10/2021",
    "02/2020",
    "06/2021",
    "07/2021",
]


import collections


def transform_buyer_detail(
    tax_code, buyer_dict, taxcode_to_name, defaut_list_date=defaut_list_date
):
    transformed_data_dict = collections.defaultdict(list)
    sellers = [key for key in (buyer_dict[tax_code]).keys() if key != "total_payment"]
    for seller in sellers:
        # AMOUNT.append(data_dict[buyer][seller]["total_amount"])
        transformed_data_dict["AMOUNT"].append(
            buyer_dict[tax_code][seller]["total_amount"]
        )
        # N_MONTH.append(len(data_dict[buyer][seller]["dates"].keys()))
        transformed_data_dict["N_INVOICE"].append(0)
        [transformed_data_dict[key].append(None) for key in defaut_list_date]
        transformed_data_dict["N_MONTH"].append(
            len(buyer_dict[tax_code][seller]["dates"].keys())
        )
        for date, value in buyer_dict[tax_code][seller]["dates"].items():
            transformed_data_dict[date][-1] = value
            transformed_data_dict["N_INVOICE"][-1] += value[-1]

        # date_dict = data_dict[buyer][seller]["dates"]
        # _n_invoice = sum(date_dict[item] for item in date_dict)
        transformed_data_dict["BUYER_TAX_CODE"].append(tax_code)
        # BUYER_TAX_CODE.append(buyer)
        transformed_data_dict["SELLER_TAX_CODE"].append(seller)
        transformed_data_dict["BUYER_NAME"].append(simplify(taxcode_to_name[tax_code]))
        transformed_data_dict["SELLER_NAME"].append(simplify(taxcode_to_name[seller]))
    return transformed_data_dict


def processed_dataframe(transformed_data_dict, min_month=6):
    processed_df = pd.DataFrame.from_dict(transformed_data_dict)
    processed_df = processed_df[
        (processed_df.BUYER_TAX_CODE != processed_df.SELLER_TAX_CODE)
    ]
    reduced_df = processed_df[(processed_df.N_MONTH > min_month)]
    return processed_df, reduced_df


def manual_clustering(amount, total_amount):
    if amount == None:
        amount = 0
    if amount >= 0.02 * total_amount:
        return "Class A"
    elif amount >= 0.005 * total_amount and amount < 0.02 * total_amount:
        return "Class B"
    elif amount >= 0.001 * total_amount and amount < 0.005 * total_amount:
        return "Class C"
    elif amount >= 0.0005 * total_amount and amount < 0.001 * total_amount:
        return "Class D"
    else:
        return "Class E"


def apply_manual_clustering(df: pd.DataFrame):
    col_class = ["UnClass"] * len(df)
    df.insert(1, "class", col_class)
    total_amount = sum(df["AMOUNT"])
    df["class"] = df["AMOUNT"].apply(lambda row: manual_clustering(row, total_amount))
    colors = [
        "#" + "".join([random.choice("0123456789ABCDEF") for j in range(6)])
        for i in range(5)
    ]
    map_class_to_index = {
        "Class A": 0,
        "Class B": 1,
        "Class C": 2,
        "Class D": 3,
        "Class E": 4,
    }
    labels = df["class"].tolist()
    list_color = [colors[map_class_to_index[i]] for i in labels]
    df.insert(1, "Color", list_color)
    df.insert(1, "label", labels)
    return df, colors


import plotly.express as px
import numpy as np

manual_title = "<b>Class A:</b> [20% , 100%] on total payment       <b>Class B:</b> [5% , 20%) on total payment     <b>Class C:</b> [1% , 5%) on total payment      <b>Class D:</b> [0.5% , 1%) on total payment        <b>Class E:</b> [0% , 0.5%) on total payment<br />"


def create_treemap_fig(df: pd.DataFrame, auto_cluster_state):
    if auto_cluster_state == False:
        title = manual_title
    else:
        title = ""
    fig = px.treemap(
        df,
        path=["BUYER_NAME", "class", "SELLER_NAME"],
        values="AMOUNT",
        color="N_MONTH",
        maxdepth=3,
        hover_data=["N_INVOICE", "SELLER_TAX_CODE"],
        title=title,
    )
    fig.update_layout(
        title={"y": 0.06, "x": 0.05, "xanchor": "left", "yanchor": "top"},
        margin=dict(l=80, r=80, t=40, b=100),
    )
    fig["layout"]["title"]["font"] = dict(size=14)

    return fig


from sklearn.cluster import KMeans
import random


def auto_cluster(df, num_cluster):
    X = []
    for i in range(len(df)):
        _amount = df.iloc[i]["AMOUNT"]
        _month = df.iloc[i]["N_MONTH"]
        _invoices = df.iloc[i]["N_INVOICE"]
        X.append([_amount, _month, _invoices])
    LIST_MAX = max(X)
    X = np.array(X)
    N = X / np.array(LIST_MAX)

    km = KMeans(n_clusters=num_cluster)
    km.fit(N)
    km.predict(N)
    labels = km.labels_

    colors = [
        "#" + "".join([random.choice("0123456789ABCDEF") for j in range(6)])
        for i in range(num_cluster)
    ]
    list_color = [colors[i] for i in labels]
    df.insert(1, "Color", list_color)
    df.insert(1, "label", labels)
    df.insert(1, "class", labels)

    return df, colors


def create_3d_plot(df, colors):
    df = df.rename(
        columns={"N_INVOICE": "Invoice", "N_MONTH": "Month", "AMOUNT": "Amount"}
    )
    fig = px.scatter_3d(
        df,
        x="Invoice",
        y="Month",
        z="Amount",
        color="Color",
        hover_data=["Amount", "Month", "Invoice", "SELLER_NAME"],
    )
    color_dict = {}
    for i, c in enumerate(colors):
        color_dict[c] = i
    fig.for_each_trace(lambda t: t.update(name=color_dict[t.name]))
    return fig


def create_column_trading(transformed_df, buyer_taxcode, seller_taxcode):
    t = transformed_df[
        (transformed_df.SELLER_TAX_CODE == seller_taxcode)
        & (transformed_df.BUYER_TAX_CODE == buyer_taxcode)
    ]
    list_month = []
    list_amount = []
    for key in t.keys():
        val = t[key].tolist()
        try:
            if "/" in key:
                val = val[0][0]
                list_month.append(datetime.strptime(key, "%m/%Y"))
                list_amount.append(val)
        except:
            print("error: Missing Value, Maybe network is not stable", val)
    vis_dict = {"month": list_month, "amount": list_amount}
    title = f"Trading between <br><b>{t.BUYER_NAME.item()}</b> and <b>{t.SELLER_NAME.item()}</b>"
    fig = px.bar(vis_dict, x="month", y="amount", title=title)
    fig.update_traces(hovertemplate="Buy: %{y}" + "<br>Month: %{x}")
    fig.update_traces(marker_color="#06b6d4")
    fig.update_layout(
        title={
            "y": 0.9,
            "x": 0.5,
            "xanchor": "center",
            "yanchor": "top",
        },
        margin=dict(l=40, r=40, t=100, b=20),
    )
    fig["layout"]["title"]["font"] = dict(size=14)
    return fig
