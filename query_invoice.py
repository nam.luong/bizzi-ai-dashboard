import os
import requests
import time
from pprint import pprint
import json


def poll_job(s, redash_url, job):
    # TODO: add timeout
    while job["status"] not in (3, 4):
        response = s.get("{}/api/jobs/{}".format(redash_url, job["id"]))
        job = response.json()["job"]
        time.sleep(1)
    print(job)
    if job["status"] == 3:
        return job["query_result_id"]

    return None


def get_fresh_query_result(redash_url, query_id, api_key, params):
    s = requests.Session()
    s.headers.update({"Authorization": "Key {}".format(api_key)})

    payload = dict(max_age=0, parameters=params)

    response = s.post(
        "{}/api/queries/{}/results".format(redash_url, query_id),
        data=json.dumps(payload),
    )
    if response.status_code != 200:
        raise Exception("Refresh failed.")

    result_id = poll_job(s, redash_url, response.json()["job"])

    if result_id:
        response = s.get(
            "{}/api/queries/{}/results/{}.json".format(redash_url, query_id, result_id)
        )
        if response.status_code != 200:
            raise Exception("Failed getting results.")
    else:
        raise Exception("Query execution failed.")

    return response.json()["query_result"]["data"]["rows"]


import datetime

if __name__ == "__main__":
    # params = {"seller_tax_code": "0313933854"}

    # base = datetime.datetime(2019, 11, 20)
    # start_list = [base + datetime.timedelta(days=x) for x in range(0, 840, 10)]
    # end_list = [s + datetime.timedelta(days=9) for s in start_list]
    # for s, e in zip(start_list, end_list):
    #     sd = s.strftime("%Y-%m-%d")
    #     ed = e.strftime("%Y-%m-%d")
    #     print(f"Start: {sd}")
    #     print(f"End: {ed}")
    #     params = {"start_date": sd, "end_date": ed}
    #     query_id = 192
    #     # Need to use a *user API key* here (and not a query API key).
    #     api_key = "Nswd0gz4RPhHpfFjt3AafrI1Ee8ACVl5WJ08q7RC"
    #     data = get_fresh_query_result(
    #         "https://stats.bizzi.vn", query_id, api_key, params
    #     )
    #     with open(f"bizzi_data/{sd}_{ed}.json", "w", encoding="utf-8") as f:
    #         json.dump(data, f, ensure_ascii=False, indent=4)
    #     print("---------------------------done------------------------------")

    base = datetime.datetime(2019, 11, 29)
    start_list = [base + datetime.timedelta(days=x) for x in range(520, 840, 10)]
    end_list = [s + datetime.timedelta(days=1) for s in start_list]
    for s, e in zip(start_list, end_list):
        sd = s.strftime("%Y-%m-%d")
        ed = e.strftime("%Y-%m-%d")
        print(f"Start: {sd}")
        print(f"End: {ed}")
        params = {"start_date": sd, "end_date": ed}
        query_id = 192
        # Need to use a *user API key* here (and not a query API key).
        api_key = "Nswd0gz4RPhHpfFjt3AafrI1Ee8ACVl5WJ08q7RC"
        data = get_fresh_query_result(
            "https://stats.bizzi.vn", query_id, api_key, params
        )
        with open(f"bizzi_data/{sd}_{ed}.json", "w", encoding="utf-8") as f:
            json.dump(data, f, ensure_ascii=False, indent=4)
        print("---------------------------done------------------------------")
