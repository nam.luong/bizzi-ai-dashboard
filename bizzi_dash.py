from glob import glob
import bizzi_util
from dash import Dash, dcc, html, Input, Output
import base64
import plotly.graph_objects as go
import pandas as pd

(
    buyer_dict,
    seller_dict,
    taxcode_to_name,
    sorted_buyer_with_amount,
    sorted_seller_with_amount,
    dict_for_year,
) = bizzi_util.load_pickle_data("combination_dict3.pickle")

# test_code = list(sorted_buyer_with_amount.keys())[1]
# print(taxcode_to_name[test_code])
list_sorted_buyer_with_amount = list(sorted_buyer_with_amount.keys())
# sorted_name = [bizzi_util.simplify(taxcode_to_name[taxcode]) for taxcode in list(sorted_buyer_with_amount.keys())]
app = Dash(external_stylesheets=["https://codepen.io/chriddyp/pen/bWLwgP.css"])

encoded_home_image = base64.b64encode(open("assets/home.png", "rb").read())
encoded_bizzi_image = base64.b64encode(open("assets/logo.png", "rb").read())
server = app.server


# app.layout = html.Div([
#     dcc.Dropdown( options=[{'value': taxcode, 'label': bizzi_util.simplify(taxcode_to_name[taxcode])} for taxcode in list_sorted_buyer_with_amount], value= 'NYC', id='demo-dropdown'),
#     html.Div(id='dd-output-container')
# ])

auto_clustering_state = False
default_taxcode = list_sorted_buyer_with_amount[0]
taxcode = default_taxcode
seller_taxcode = None


def set_default_graph(default_taxcode=default_taxcode):
    transform_data_dict = bizzi_util.transform_buyer_detail(
        tax_code=default_taxcode, buyer_dict=buyer_dict, taxcode_to_name=taxcode_to_name
    )
    processed_df, reduced_df = bizzi_util.processed_dataframe(transform_data_dict)
    # df = bizzi_util.manual_clustering(reduced_df)
    df, colors = bizzi_util.apply_manual_clustering(reduced_df)
    treemap_fig = bizzi_util.create_treemap_fig(df, auto_clustering_state)
    plot_fig = bizzi_util.create_3d_plot(df, colors)

    return treemap_fig, plot_fig


default_treemap_fig, default_3d_plot_fig = set_default_graph(default_taxcode)
default_treemap_fig.update_layout(
    title={
        "text": bizzi_util.manual_title,
        "y": 0.05,
        "x": 0.05,
        "xanchor": "left",
        "yanchor": "top",
    },
)
default_treemap_fig["layout"]["title"]["font"] = dict(size=14)


app.title = "Bizzi Dash"
app.layout = html.Div(
    style={"backgroundColor": "#ebeff1", "width": "100%"},
    children=[
        html.Div(
            className="row",
            style={
                "display": "inline-block",
                "background-color": "#ffffff",
                "width": "100%",
                "height": "60px",
            },
            children=[
                html.Div(
                    className="one columns",
                    children=[
                        html.Img(
                            src="data:image/png;base64,{}".format(
                                encoded_bizzi_image.decode()
                            ),
                            style={
                                "margin-left": "50px",
                                "display": "inline-block",
                            },
                        ),
                    ],
                    style={"display": "inline-block"},
                ),
            ],
        ),
        html.Div(
            className="row",
            style={
                "display": "inline-block",
                "margin-top": "10px",
                "margin-left": "50px",
            },
            children=[
                html.H1(
                    "Dashboard",
                    style={
                        "fontSize": 40,
                        "color": "#485057",
                        # "margin-left": "50px",
                        "margin-top": "15px",
                        "width": "200px",
                        "display": "center",
                        # 'font-weight': 'bold',
                    },
                    # className="four columns",
                ),
                # ],),
                html.Div(
                    className="row",
                    style={
                        "display": "inline-block",
                        # "margin-top": "10px",
                        # "margin-left": "50px",
                    },
                    children=[
                        dcc.Dropdown(
                            options=[
                                {
                                    "value": taxcode,
                                    "label": bizzi_util.simplify(
                                        taxcode_to_name[taxcode]
                                    ),
                                }
                                for taxcode in list_sorted_buyer_with_amount
                            ],
                            value=default_taxcode,
                            id="demo-dropdown",
                            style={
                                # "margin-left": "25px",
                                # "margin-top": "10px",
                                "width": "700px",
                                "display": "inline-block",
                            },
                            # className="three columns",
                        ),
                        html.Div(
                            className="row",
                            style={
                                "display": "center",
                                # "margin-top": "10px",
                                # "margin-left": "50px",
                            },
                            children=[
                                html.H1(
                                    # " >   " + seller_df["buyer_legal_name"][0], HERE
                                    " >   " + "?",
                                    id="h1-selected-container",
                                    style={
                                        "fontSize": 20,
                                        "color": "#485057",
                                        # "margin-left": "10px",
                                        "margin-top": "10px",
                                        "text-decoration": "underline",
                                    },
                                    # className="row",
                                ),
                            ],
                        ),
                    ],
                ),
            ],
        ),
        html.Div(
            className="row",
            children=[
                html.Div(
                    className="one columns",
                    style={
                        "display": "inline-block",
                        "border-radius": "5px",
                        "box-shadow": "2px 2px 2px lightgrey",
                        "background-color": "#ffffff",
                        "width": "13%",
                        "height": "135px",
                        "margin-top": "10px",
                        "margin-left": "50px",
                        "display": "inline-block",
                        "border-radius": "5px",
                        "box-shadow": "2px 2px 2px lightgrey",
                        "background-color": "#ffffff",
                    },
                    children=[
                        html.H1(
                            "?",
                            id="h1-total-payment",
                            style={
                                "textAlign": "center",
                                "fontSize": 26,
                                "color": "#47B39C",
                                # 'font-style': 'bold',
                                "font-weight": "bold",
                                "margin-top": "35px",
                            },
                        ),
                        html.H1(
                            "Total Payment",
                            style={
                                "textAlign": "center",
                                "fontSize": 20,
                                "color": "#485057",
                            },
                        ),
                    ],
                ),
                html.Div(
                    className="one columns",
                    style={
                        "display": "inline-block",
                        "textAlign": "center",
                        "border-radius": "5px",
                        "box-shadow": "2px 2px 2px lightgrey",
                        "background-color": "#ffffff",
                        "width": "13%",
                        "height": "135px",
                        "margin-top": "10px",
                        "margin-left": "30px",
                        "border-radius": "5px",
                        "box-shadow": "2px 2px 2px lightgrey",
                        "background-color": "#ffffff",
                    },
                    children=[
                        html.H1(
                            "Payment in 2020",
                            style={
                                "display": "newline",
                                "textAlign": "center",
                                "fontSize": 16,
                                "color": "#485057",
                                # "width": "100%",
                                "height": "3px",
                                # 'font-style': 'bold',
                                # 'font-weight': 'bold',
                                "margin-top": "15px",
                                # 'margin-right': '15px',
                            },
                        ),
                        html.H1(
                            # n_active, HERE
                            "?",
                            id="h1-payment-2020",
                            style={
                                "display": "newline",
                                "textAlign": "center",
                                "fontSize": 26,
                                # "verticalAlign" : "middle",
                                "color": "#485057",
                                # 'font-style': 'bold',
                                "font-weight": "bold",
                                "margin-top": "20px",
                                "height": "25px",
                                # "margin-top":"40px"
                            },
                            className="row",
                        ),
                        html.H1(
                            # f"({round((n_active/n_company)*100)}%)",HERE
                            f"(?)",
                            id="h1-payment-2020-percent",
                            style={
                                # "display": "inline",
                                "textAlign": "center",
                                "fontSize": 16,
                                "color": "#EC6B56",
                                # "width": "100%",
                                "height": "3px",
                                # 'font-style': 'bold',
                                "font-weight": "bold",
                                "margin-top": "-5px",
                                # 'margin-right': '15px',
                            },
                        ),
                        html.H1(
                            "Compare to previous year",
                            style={
                                "display": "newline",
                                "textAlign": "center",
                                "fontSize": 16,
                                "color": "#EC6B56",
                            },
                        ),
                    ],
                ),
                html.Div(
                    className="one columns",
                    style={
                        "display": "inline-block",
                        "textAlign": "center",
                        "border-radius": "5px",
                        "box-shadow": "2px 2px 2px lightgrey",
                        "background-color": "#ffffff",
                        "width": "13%",
                        "height": "135px",
                        "margin-top": "10px",
                        "margin-left": "30px",
                        "display": "inline-block",
                        "border-radius": "5px",
                        "box-shadow": "2px 2px 2px lightgrey",
                        "background-color": "#ffffff",
                    },
                    children=[
                        html.H1(
                            "Payment in 2021",
                            style={
                                "display": "newline",
                                "textAlign": "center",
                                "fontSize": 16,
                                "color": "#485057",
                                "height": "3px",
                                "margin-top": "15px",
                            },
                        ),
                        html.H1(
                            # n_unk,HERE
                            "?",
                            id="h1-payment-2021",
                            style={
                                "display": "newline",
                                "textAlign": "center",
                                "fontSize": 26,
                                "color": "#485057",
                                "font-weight": "bold",
                                "margin-top": "20px",
                                "height": "25px",
                            },
                        ),
                        html.H1(
                            # f"({round((n_unk/n_company)*100)}%)",
                            f"(?)",
                            id="h1-payment-2021-percent",
                            style={
                                "textAlign": "center",
                                "fontSize": 16,
                                "color": "#47B39C",
                                "height": "3px",
                                "font-weight": "bold",
                                "margin-top": "-5px",
                            },
                        ),
                        html.H1(
                            "Compare to previous year",
                            style={
                                "display": "newline",
                                "textAlign": "center",
                                "fontSize": 16,
                                "color": "#47B39C",
                            },
                        ),
                    ],
                ),
                html.Div(
                    className="one columns",
                    style={
                        "display": "inline-block",
                        "border-radius": "5px",
                        "box-shadow": "2px 2px 2px lightgrey",
                        "background-color": "#ffffff",
                        "width": "13%",
                        "height": "135px",
                        "margin-top": "10px",
                        "margin-left": "30px",
                        "display": "inline-block",
                        "border-radius": "5px",
                        "box-shadow": "2px 2px 2px lightgrey",
                        "background-color": "#ffffff",
                    },
                    children=[
                        html.H1(
                            # num_customer, HERE
                            "?",
                            id="h1-number-supplier",
                            style={
                                "textAlign": "center",
                                "fontSize": 35,
                                "color": "#485057",
                                "font-weight": "bold",
                                "margin-top": "35px",
                            },
                        ),
                        html.H1(
                            "Suppliers",
                            style={
                                "textAlign": "center",
                                "fontSize": 20,
                                "color": "#485057",
                            },
                        ),
                    ],
                ),
                html.Div(
                    className="one columns",
                    style={
                        "display": "inline-block",
                        "border-radius": "5px",
                        "box-shadow": "2px 2px 2px lightgrey",
                        "background-color": "#ffffff",
                        "width": "13%",
                        "height": "135px",
                        "margin-top": "10px",
                        "margin-left": "30px",
                        "display": "inline-block",
                        "border-radius": "5px",
                        "box-shadow": "2px 2px 2px lightgrey",
                        "background-color": "#ffffff",
                    },
                    children=[
                        html.H1(
                            # num_customer, HERE
                            "?",
                            id="h1-regular-supplier",
                            style={
                                "textAlign": "center",
                                "fontSize": 35,
                                "color": "#485057",
                                "font-weight": "bold",
                                "margin-top": "35px",
                            },
                        ),
                        html.H1(
                            "Regular suppliers",
                            style={
                                "textAlign": "center",
                                "fontSize": 20,
                                "color": "#485057",
                            },
                        ),
                    ],
                ),
                html.Div(
                    className="one columns",
                    style={
                        "display": "inline-block",
                        "border-radius": "5px",
                        "box-shadow": "2px 2px 2px lightgrey",
                        "background-color": "#ffffff",
                        "width": "13%",
                        "height": "135px",
                        "margin-top": "10px",
                        "margin-left": "30px",
                        "border-radius": "5px",
                        "box-shadow": "2px 2px 2px lightgrey",
                        "background-color": "#ffffff",
                    },
                    children=[
                        html.H1(
                            # num_customer, HERE
                            "?",
                            id="h1-regular-supplier-percentage",
                            style={
                                "textAlign": "center",
                                "fontSize": 35,
                                "color": "#485057",
                                "font-weight": "bold",
                                "margin-top": "35px",
                            },
                        ),
                        html.H1(
                            "Payment for regular supplier",
                            style={
                                "textAlign": "center",
                                "fontSize": 20,
                                "color": "#485057",
                            },
                        ),
                    ],
                ),
            ],
        ),
        html.Div(
            className="row",
            style={
                "display": "inline-block",
                "margin-left": "50px",
                "margin-top": "30px",
                "width": "85%",
                "height": "800px",
            },
            children=[
                dcc.Checklist(
                    ["Auto clustering"],
                    [],
                    id="check-list-auto-clustering",
                    inline=True,
                ),
                html.H1(
                    "Treemap",
                    style={
                        "textAlign": "center",
                        "fontSize": 20,
                        "color": "#485057",
                        "margin-top": "50px",
                    },
                ),
                dcc.Graph(
                    id="treemap-chart",
                    style={
                        "display": "center",
                        "margin-left": "5px",
                        "margin-top": "5px",
                        # "margin-bottom": "-10px",
                        # "width": "80%",
                        "height": "800px",
                    },
                    figure=default_treemap_fig,
                    config={
                        "displaylogo": False,
                        "scrollZoom": True,
                    },
                ),
                html.Div(
                    className="row",
                    style={
                        "display": "inline-block",
                        # "margin-left": "50px",
                        "margin-top": "20px",
                        "width": "100%",
                        # "height": "800px",
                    },
                    children=[
                        html.Div(
                            className="two columns",
                            style={
                                "display": "inline-block",
                                # "margin-left": "50px",
                                # "margin-top": "100px",
                                "width": "48%",
                                # "height": "600px",
                            },
                            children=[
                                html.H1(
                                    "Trading Column chart",
                                    style={
                                        "textAlign": "center",
                                        "fontSize": 20,
                                        "color": "#485057",
                                    },
                                ),
                                html.Button('Reload Column Chart', id='reload-column-chart', n_clicks=0),
                                dcc.Graph(
                                    id="column-chart",
                                    style={
                                        "display": "center",
                                        "margin-left": "5px",
                                        "margin-top": "5px",
                                        # "margin-bottom": "-10px",
                                        # "width": "80%",
                                        "height": "600px",
                                    },
                                    figure=go.Figure(data=[go.Scatter(x=[], y=[])]),
                                    config={
                                        "displaylogo": False,
                                        "scrollZoom": True,
                                    },
                                ),
                            ],
                        ),
                        html.Div(
                            className="two columns",
                            style={
                                "display": "inline-block",
                                # "margin-left": "50px",
                                # "margin-top": "100px",
                                "width": "48%",
                            },
                            children=[
                                html.H1(
                                    "3D scatter",
                                    style={
                                        "textAlign": "center",
                                        "fontSize": 20,
                                        "color": "#485057",
                                    },
                                ),
                                dcc.Graph(
                                    id="3D-plot-chart",
                                    style={
                                        "display": "center",
                                        "margin-left": "5px",
                                        "margin-top": "60px",
                                        # "margin-bottom": "-10px",
                                        # "width": "80%",
                                        "height": "600px",
                                    },
                                    figure=default_3d_plot_fig,
                                    config={
                                        "displaylogo": False,
                                        "scrollZoom": True,
                                    },
                                ),
                            ],
                        ),
                    ],
                ),
            ],
        ),
    ],
)

n_cluster = 10

@app.callback(
    [
        Output("h1-selected-container", "children"),
        Output("h1-total-payment", "children"),
        Output("h1-payment-2020", "children"),
        Output("h1-payment-2021", "children"),
        Output("h1-payment-2021-percent", "children"),
        Output("h1-number-supplier", "children"),
        Output("h1-regular-supplier", "children"),
        Output("h1-regular-supplier-percentage", "children"),
        Output("treemap-chart", "figure"),
        Output("3D-plot-chart", "figure"),
    ],
    [
        Input("demo-dropdown", "value"),
        Input("check-list-auto-clustering", "value"),
    ],
)
def update_output(drop_down_value, checklist_value):
    # print(drop_down_value)
    global taxcode
    global auto_clustering_state
    global n_cluster
    if drop_down_value == None:
        taxcode = default_taxcode
    else:
        taxcode = drop_down_value

    if "Auto clustering" in checklist_value:
        auto_clustering_state = True
    else:
        auto_clustering_state = False

    transform_data_dict = bizzi_util.transform_buyer_detail(
        tax_code=taxcode, buyer_dict=buyer_dict, taxcode_to_name=taxcode_to_name
    )
    processed_df, reduced_df = None, None
    list_month_reduce = [6, 3, 1]
    for m in list_month_reduce:
        processed_df, reduced_df = bizzi_util.processed_dataframe(
            transform_data_dict, m
        )
        if len(reduced_df) >= 10:
            break
    if len(reduced_df) < 10:
        n_cluster = len(reduced_df)
    # print(sum(processed_df.AMOUNT))
    name = ""
    num_regular_supplier = len(reduced_df)

    total_payment = 0
    name = f"{taxcode_to_name[taxcode]}"
    regular_payment = sum(reduced_df.AMOUNT)
    total_payment = sum(processed_df.AMOUNT)
    percentage_regular = round((regular_payment / total_payment) * 100)

    total_payment = format(int(total_payment), ",d")

    payment_2020 = dict_for_year[taxcode]["2020"]
    payment_2021 = dict_for_year[taxcode]["2021"]
    if payment_2020 == 0:
        percent_2021_2020 = "+∞"
    else:
        percent_2021_2020 = round(((payment_2021 - payment_2020) / payment_2020) * 100)
        if percent_2021_2020 > 0:
            percent_2021_2020 = f"+{percent_2021_2020}%"
        else:
            percent_2021_2020 = f"-{percent_2021_2020}%"

    payment_2020 = format(int(payment_2020), ",d")
    payment_2021 = format(int(payment_2021), ",d")
    num_supplier = len(buyer_dict[taxcode].keys()) - 1
    # print("GO IN: ",auto_clustering_state)
    if auto_clustering_state == False:
        df, colors = bizzi_util.apply_manual_clustering(reduced_df)
        treemap_fig = bizzi_util.create_treemap_fig(df, auto_clustering_state)
        plot_3d_fig = bizzi_util.create_3d_plot(df, colors)
    else:
        df, colors = bizzi_util.auto_cluster(reduced_df, n_cluster)
        treemap_fig = bizzi_util.create_treemap_fig(df, auto_clustering_state)
        plot_3d_fig = bizzi_util.create_3d_plot(df, colors)

    return (
        f">   {name} - MST: {taxcode}",
        total_payment,
        payment_2020,
        payment_2021,
        percent_2021_2020,
        num_supplier,
        num_regular_supplier,
        f"{percentage_regular}%",
        treemap_fig,
        plot_3d_fig,
    )


@app.callback(Output("column-chart", "figure"),

[Input("treemap-chart", "clickData"),
Input('reload-column-chart', 'n_clicks')])
def supplier_selection(click, reload_click):
    global taxcode
    global auto_clustering_state
    global seller_taxcode
    column_fig = go.Figure(data=[go.Scatter(x=[], y=[])])
    if click != None:
        if click["points"][0]["customdata"][1] != "(?)":
            seller_taxcode = click["points"][0]["customdata"][1]
        else:
            seller_taxcode = "(?)"
    if seller_taxcode!= "(?)" and seller_taxcode!= None:
        transform_data_dict = bizzi_util.transform_buyer_detail(
            tax_code=taxcode,
            buyer_dict=buyer_dict,
            taxcode_to_name=taxcode_to_name,
        )
        transform_df = pd.DataFrame.from_dict(transform_data_dict)
        for key in transform_df.keys():
            if "/" in key:
                transform_df[key] = transform_df[key].apply(lambda d: d if isinstance(d, list) else [0,0])
        column_fig = bizzi_util.create_column_trading(
            transformed_df=transform_df,
            buyer_taxcode=taxcode,
            seller_taxcode=seller_taxcode,
        )
    return column_fig
    

if __name__ == "__main__":
    app.run_server(debug=True)
